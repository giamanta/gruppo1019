# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140204095807) do

  create_table "acts", force: true do |t|
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", force: true do |t|
    t.string   "name"
    t.string   "rs"
    t.string   "web"
    t.integer  "referent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "describes", force: true do |t|
    t.integer  "smash_id"
    t.integer  "report_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "has", force: true do |t|
    t.integer  "idreport"
    t.integer  "idmitigation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "includes", force: true do |t|
    t.integer  "act_id"
    t.integer  "mitigation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "machines", force: true do |t|
    t.string   "hostname"
    t.string   "os"
    t.string   "version"
    t.string   "type"
    t.string   "role"
    t.string   "ip"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
    t.string   "state"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "malwares", force: true do |t|
    t.string   "family"
    t.string   "version"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mitigations", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.time     "exectime"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positions", force: true do |t|
    t.string   "latitude"
    t.string   "longitude"
    t.string   "ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "referents", force: true do |t|
    t.string  "name"
    t.string  "surname"
    t.integer "via"
    t.integer "civico"
    t.string  "comune"
    t.integer "cap"
    t.string  "email"
    t.integer "company_id"
  end

  create_table "removals", force: true do |t|
    t.string   "familyname"
    t.integer  "mitigation_id"
    t.integer  "malware_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: true do |t|
    t.string   "title"
    t.integer  "score"
    t.integer  "machine_id"
    t.integer  "referent_id"
    t.integer  "technician_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "smashes", force: true do |t|
    t.string   "permalink"
    t.string   "impact"
    t.string   "state"
    t.integer  "malware_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "technicians", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.integer  "via"
    t.integer  "civico"
    t.string   "comune"
    t.integer  "cap"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string  "name"
    t.string  "surname"
    t.integer "via"
    t.integer "civico"
    t.string  "comune"
    t.integer "cap"
    t.string  "email"
    t.integer "report_id"
  end

  create_table "uses", force: true do |t|
    t.integer  "user_id"
    t.integer  "machine_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
