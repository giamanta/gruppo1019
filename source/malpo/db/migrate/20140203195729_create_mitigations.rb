class CreateMitigations < ActiveRecord::Migration
  def change
    create_table :mitigations do |t|
      t.string :title
      t.text :description
      t.time :exectime

      t.timestamps
    end
  end
end
