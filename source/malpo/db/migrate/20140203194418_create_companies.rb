class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :rs
      t.string :web
      t.belongs_to :referent

      t.timestamps
    end
  end
end
