class CreateHas < ActiveRecord::Migration
  def change
    create_table :has do |t|
      t.integer :idreport
      t.integer :idmitigation

      t.timestamps
    end
  end
end
