class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.integer :via
      t.integer :civico
      t.string :comune
      t.integer :cap
      t.string :email
      t.belongs_to :report
    end
  end
end
