class CreateIncludes < ActiveRecord::Migration
  def change
    create_table :includes do |t|
      t.belongs_to :act
      t.belongs_to :mitigation

      t.timestamps
    end
  end
end
