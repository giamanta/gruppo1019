class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.string :hostname
      t.string :os
      t.string :version
      t.string :type
      t.string :role
      t.string :ip
      t.float :latitude
      t.float :longitude
      t.string :city
      t.string :state
      t.belongs_to :company

      t.timestamps
    end
  end
end
