class CreateUses < ActiveRecord::Migration
  def change
    create_table :uses do |t|
      t.belongs_to :user
      t.belongs_to :machine

      t.timestamps
    end
  end
end
