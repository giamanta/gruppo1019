class CreateTechnicians < ActiveRecord::Migration
  def change
    create_table :technicians do |t|
      t.string :name
      t.string :surname
      t.integer :via
      t.integer :civico
      t.string :comune
      t.integer :cap
      t.string :email

      t.timestamps
    end
  end
end
