class CreateReferents < ActiveRecord::Migration
  def change
    create_table :referents do |t|
      t.string :name
      t.string :surname
      t.integer :via
      t.integer :civico
      t.string :comune
      t.integer :cap
      t.string :email
      t.belongs_to :company
    end
  end
end
