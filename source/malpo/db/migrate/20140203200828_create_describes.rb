class CreateDescribes < ActiveRecord::Migration
  def change
    create_table :describes do |t|
      t.belongs_to :smash
      t.belongs_to :report

      t.timestamps
    end
  end
end
