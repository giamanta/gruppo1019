class CreateRemovals < ActiveRecord::Migration
  def change
    create_table :removals do |t|
      t.string :familyname
      t.belongs_to :mitigation
      t.belongs_to :malware

      t.timestamps
    end
  end
end
