class CreateSmashes < ActiveRecord::Migration
  def change
    create_table :smashes do |t|
      t.string :permalink
      t.string :impact
      t.string :state
      t.belongs_to :malware

      t.timestamps
    end
  end
end
