class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :title
      t.integer :score
      t.belongs_to :machine
      t.belongs_to :referent
      t.belongs_to :technician

      t.timestamps
    end
  end
end
