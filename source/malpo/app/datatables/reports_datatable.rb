class ReportsDatatable
  delegate :params, :h, :link_to, :number_to_human, :number_with_precision, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Report.count,
      iTotalDisplayRecords: reports.total_entries,
      aaData: data
    }
  end

private

  def data
    reports.map do |report|
      [
        link_to(report.id, report),
        h(report.title),
        number_to_human(report.score),
        report.technician_id,
        report.machine_id,
        report.referent_id,
        h(report.created_at.strftime("%B %e, %Y")),
      ]
    end
  end

  def reports
    @reports ||= fetch_reports
  end

  def fetch_reports
    reports = Report.order("#{sort_column} #{sort_direction}")
    reports = reports.page(page).per_page(per_page)
    if params[:sSearch].present?
      reports = reports.where("title like :search or id like :search", search: "%#{params[:sSearch]}%")
    end
    reports
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id title score idtech hostname ip released]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
