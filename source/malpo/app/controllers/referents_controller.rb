class ReferentsController < ApplicationController
    def new
        @referent = Referent.new
    end

    def create
        @referent = Referent.new(referent_params)
        if @referent.save
            redirect_to @referent
        else
            render 'new'
        end
    end

    def show
        @referent = Referent.find(params[:id])
    end

    def index
        @referents = Referent.all
    end

    def edit
        @referent = Referent.find(params[:id])
    end

    def update
        @referent = Referent.find(params[:id])

        if @referent.update(referent_params)
            redirect_to @referent
        else
            render 'edit'
        end
    end

    def destroy
        @referent = Referent.find(params[:id])
        @referent.destroy

        redirect_to referents_path
    end

    # Strong parameters
    private
    def referent_params
        params.require(:post).permit(:name, :surname, :via, :civico, :comune, :cap, :email, :idcompany)
    end
end
