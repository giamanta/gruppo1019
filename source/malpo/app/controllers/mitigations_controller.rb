class MitigationsController < ApplicationController
    def new
        @mitigation = Mitigation.new
    end

    def create
        @mitigation = Mitigation.new(mitigation_params)
        if @mitigation.save
            redirect_to @mitigation
        else
            render 'new'
        end
    end

    def show
        @mitigation = Mitigation.find(params[:id])
    end

    def index
        @mitigations = Mitigation.all
    end

    def edit
        @mitigation = Mitigation.find(params[:id])
    end

    def update
        @mitigation = Mitigation.find(params[:id])

        if @mitigation.update(mitigation_params)
            redirect_to @mitigation
        else
            render 'edit'
        end
    end

    def destroy
        @mitigation = Mitigation.find(params[:id])
        @mitigation.destroy

        redirect_to mitigations_path
    end

    # Strong parameters
    private
    def mitigation_params
        params.require(:mitigation).permit()
    end
end
