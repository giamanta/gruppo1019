class SmashesController < ApplicationController
    def new
        @smash = Smash.new
    end

    def create
        @smash = Smash.new(smash_params)
        if @smash.save
            redirect_to @smash
        else
            render 'new'
        end
    end

    def show
        @smash = Smash.find(params[:id])
    end

    def index
        @smashes = Smash.all
    end

    def edit
        @smash = Smash.find(params[:id])
    end

    def update
        @smash = Smash.find(params[:id])

        if @smash.update(smash_params)
            redirect_to @smash
        else
            render 'edit'
        end
    end

    def destroy
        @smash = Smash.find(params[:id])
        @smash.destroy

        redirect_to smashes_path
    end

    # Strong parameters
    private
    def smash_params
        params.require(:smash).permit()
    end
end
