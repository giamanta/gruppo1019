class Company < ActiveRecord::Base
    belongs_to :referent
    has_many :machines
end
