class User < ActiveRecord::Base
    belongs_to :report
    has_many :uses
    has_many :machines, through: :uses
end
