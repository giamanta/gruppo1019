class Report < ActiveRecord::Base
 validates :title, presence: true, length: { minimum: 5 }
 validates :score, presence: true
 validates :technician_id, presence: true
 validates :machine_id, presence: true
 validates :referent_id, presence: true
 belongs_to :technician
 belongs_to :referent
 belongs_to :machine
 has_many :users
 has_many :has
 has_many :mitigations, through: :has
 has_many :describes
 has_many :smashes, through: :describes
end
