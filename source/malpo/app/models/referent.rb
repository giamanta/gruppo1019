class Referent < ActiveRecord::Base
    belongs_to :company
    has_many :reports
end
