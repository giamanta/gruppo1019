class Mitigation < ActiveRecord::Base
    has_many :has
    has_many :reports, through: :has
    has_many :includes
    has_many :acts, through: :includes
    has_many :removals
    has_many :malwares, through: :removals
end
