class Act < ActiveRecord::Base
    has_many :includes
    has_many :mitigations, through: :includes
end
