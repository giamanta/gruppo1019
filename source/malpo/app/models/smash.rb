class Smash < ActiveRecord::Base
    belongs_to :malware
    has_many :describes
    has_many :reports, through: :describes
end
