class Machine < ActiveRecord::Base
    belongs_to :company
    has_many :reports
    has_many :uses
    has_many :users, through: :uses
end
