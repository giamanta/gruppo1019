--------------------------------------------------------------------------------

  "Progetto di una base di dati per la gestione di report tecnici sui Malware"

--------------------------------------------------------------------------------
Il seguente progetto e' stato sviluppato interamente da Giacomo Mantani, per
lo svolgimento dell'esame di Base Di Dati anno 2013/2014.
E' distribuito sotto licenza BSD, vedi in dettaglio LICENSE.txt nella root
della directory.

      ./rel/         : Directory contenente la relazione in formato HTML
      ./db/          : Directory contenente il DATABASE
      ./source/      : Directory contenente i sorgenti del site web front/end
      ./LICENSE.TXT  : Licenze BSD-2Cluses
      ./readme.txt   : This readme

--------------------------------------------------------------------------------
